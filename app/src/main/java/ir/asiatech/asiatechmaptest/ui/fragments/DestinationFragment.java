package ir.asiatech.asiatechmaptest.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import ir.asiatech.asiatechmaptest.R;

public class DestinationFragment extends Fragment {

    private TextView txvOriginAddress,txvOriginSubrub;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_destination,container,false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView(view);
        setAddress();
    }


    private void initView(View view){
        txvOriginAddress = view.findViewById(R.id.txv_address_origin);
        txvOriginSubrub = view.findViewById(R.id.txv_suburb_origin);
    }

    private void setAddress(){
        Bundle bundle = getArguments();
        if (bundle ==null) return;
        txvOriginAddress.setText(bundle.getString("address"));
        txvOriginSubrub.setText(bundle.getString("suburb"));
    }



}
