package ir.asiatech.asiatechmaptest.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import ir.asiatech.asiatechmaptest.R;
import ir.asiatech.asiatechmaptest.data.model.PlaceAddress;
import ir.asiatech.asiatechmaptest.ui.StateInterfaces;

public class OriginFragment  extends Fragment {

    private TextView txvAddress, txvSubrub;
    private Button btnAccteptOrigin;
    private Context mContext;
    private StateInterfaces.OnAcceptButtonClickListener onAcceptButtonClickListener;
    private PlaceAddress placeAddress;
    private boolean isLoading = true;
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_origin, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        initView(view);
        setListener();
        setLoading();
        super.onViewCreated(view, savedInstanceState);
    }


    private void initView(View view) {
        txvAddress = view.findViewById(R.id.txv_address);
        txvSubrub = view.findViewById(R.id.txv_suburb);
        btnAccteptOrigin = view.findViewById(R.id.btn_accept);

    }
    private void setListener(){
        btnAccteptOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isLoading)
                onAcceptButtonClickListener.onAccpetClick();
            }
        });
    }

    public void setAddress(PlaceAddress placeAddress){
        if (placeAddress.getRoad() == null || placeAddress.getNeighbourhood() == null || placeAddress.getSuburb() ==null) return;
        btnAccteptOrigin.setEnabled(true);
        this.placeAddress = placeAddress;
        isLoading = false;
        txvSubrub.setVisibility(View.VISIBLE);
        String address = placeAddress.getNeighbourhood()+" "+placeAddress.getRoad();
        txvAddress.setText(address);
        txvSubrub.setText(placeAddress.getSuburb());
    }

    public void setLoading(){
        isLoading = true;
        btnAccteptOrigin.setEnabled(false);
        txvSubrub.setVisibility(View.GONE);
        txvAddress.setText(mContext.getString(R.string.loading));
    }

    public void setOnAcceptButtonClickListener(StateInterfaces.OnAcceptButtonClickListener onAcceptButtonClickListener) {
        this.onAcceptButtonClickListener = onAcceptButtonClickListener;
    }
}
