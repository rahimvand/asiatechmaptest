package ir.asiatech.asiatechmaptest.ui.activities;

import android.annotation.SuppressLint;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import ir.asiatech.asiatechmaptest.R;
import ir.asiatech.asiatechmaptest.data.model.Place;
import ir.asiatech.asiatechmaptest.data.model.PlaceAddress;
import ir.asiatech.asiatechmaptest.data.viewmodel.PlaceViewModel;
import ir.asiatech.asiatechmaptest.ui.StateInterfaces;
import ir.asiatech.asiatechmaptest.ui.fragments.DestinationFragment;
import ir.asiatech.asiatechmaptest.ui.fragments.OriginFragment;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener, StateInterfaces.OnAcceptButtonClickListener {

    private final static int LOCATION_PERMISSION_REQUEST_CODE = 201;
    private GoogleMap mMap;
    private boolean isMapReady = false;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationCallback locationCallback;
    private String tag = "location";
    private boolean requestingLocationUpdates = true;
    private final static String REQUEST_LOCATION_INSTANCE_KEY = "location";
    private String originState = "origin";
    private String destinationState = "destination";

    private final static int REQUEST_CHECK_SETTING_CODE = 202;
    private LocationRequest locationRequest;
    private boolean moveOnChange = true;
    private Location mLocation;
    private OriginFragment originFragment;
    private DestinationFragment destinationFragment;
    private String state = "";
    private DisposableSingleObserver<Place> disposableSingleObserver;
    private PlaceViewModel placeViewModel;
    private FloatingActionButton fabBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        placeViewModel = ViewModelProviders.of(this).get(PlaceViewModel.class);
        updateValueFromBundleState(savedInstanceState);

        initFabBack();
        setOriginFragment();

    }

    private void initFabBack(){
        fabBack = findViewById(R.id.fab_back);
        fabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
    private void updateValueFromBundleState(Bundle savedInstanceState) {
        if (savedInstanceState == null) return;
        Log.v(tag, "onSaveInstanceState");
        if (savedInstanceState.keySet().contains(REQUEST_LOCATION_INSTANCE_KEY)) {
            requestingLocationUpdates = savedInstanceState.getBoolean(
                    REQUEST_LOCATION_INSTANCE_KEY
            );
            Log.v(tag, "onSaveInstanceState $requestingLocationUpdates");

        }
    }

    private void initFusedLocationClient() {

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (!checkPermission()) {
            return;
        }
        Task<Location> lastLocation = mFusedLocationProviderClient.getLastLocation();
        lastLocation.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                mLocation = location;
                if (location != null && isMapReady) {
                    showMyLocationOnMap();
                }
            }
        });
        lastLocation.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void initLocationCallBack() {

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                if (locationResult.getLastLocation() == null) {
                    return;
                }
                mLocation = locationResult.getLastLocation();

                if (moveOnChange) {
                    showMyLocationOnMap();
                    moveOnChange = false;
                }

                Log.v(tag, "location is " + locationResult.getLastLocation().getLatitude());
            }

            @Override
            public void onLocationAvailability(LocationAvailability locationAvailability) {
                super.onLocationAvailability(locationAvailability);
                moveOnChange = true;
                if (locationAvailability.isLocationAvailable()) {
                    Log.v(tag, "location is available");
                    showMyLocationOnMap();
                } else {
                    Log.v(tag, "location is not available");
                }
            }
        };
    }

    private void createLocationRequest() {
        try {
            if (locationRequest == null) {
                locationRequest = LocationRequest.create();
                locationRequest.setInterval(10000);
                locationRequest.setFastestInterval(5000);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            }
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(locationRequest);
            checkLocationSettingSatisfied(builder);
        } catch (Throwable t) {
            t.printStackTrace();
            Log.v(tag, "failed to create Location request");
        }
    }

    private void checkLocationSettingSatisfied(LocationSettingsRequest.Builder builder) {

        SettingsClient client = LocationServices.getSettingsClient(this.getApplicationContext());
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                //startLocationUpdates();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();

                //location setting are not satisfied
                //check exception
                if (e instanceof ResolvableApiException) {
                    try {
                        //show dialog with description to user
                        //TODO add dialog
                        ((ResolvableApiException) e).startResolutionForResult(
                                MapsActivity.this,
                                REQUEST_CHECK_SETTING_CODE
                        );
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                    Log.e(tag, "createLocation onFailure called");
                }
            }
        });


    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(REQUEST_LOCATION_INSTANCE_KEY, requestingLocationUpdates);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (requestingLocationUpdates) {
            startLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void startLocationUpdates() {
        if (mFusedLocationProviderClient == null) return;
        if (checkPermission())
            mFusedLocationProviderClient.requestLocationUpdates(locationRequest,
                    locationCallback,
                    null);
    }

    private void stopLocationUpdates() {
        mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
    }

    @SuppressLint("MissingPermission")
    private void showMyLocationOnMap() {
        mMap.setMyLocationEnabled(true);

        if (mLocation != null) {
            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f));
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        isMapReady = true;
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setOnCameraIdleListener(this);
        mMap.setOnCameraMoveListener(this);
        initFusedLocationClient();
        initLocationCallBack();
        createLocationRequest();
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
                ) {
            return true;
        } else {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE
            );
            return false;
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission allowed
                    initFusedLocationClient();
                    initLocationCallBack();
                    createLocationRequest();
                } else {
                    //permission denied

                }
            }
        }
    }


    private void setOriginFragment() {
        originFragment = new OriginFragment();
        originFragment.setOnAcceptButtonClickListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.frl_main, originFragment).commitAllowingStateLoss();
        state = originState;
        fabBack.setVisibility(View.GONE);
    }

    private void setDestinationFragment(PlaceAddress placeAddress) {
        if (placeAddress.getRoad() == null || placeAddress.getNeighbourhood() == null || placeAddress.getSuburb() ==null){
            Toast.makeText(this,getString(R.string.wrongAddress),Toast.LENGTH_SHORT).show();
            return;
        }

        destinationFragment = new DestinationFragment();
        Bundle bundle = new Bundle();
        bundle.putString("suburb",placeAddress.getSuburb());
        bundle.putString("address",placeAddress.getNeighbourhood()+" "+placeAddress.getRoad());
        destinationFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.frl_main, destinationFragment).commitAllowingStateLoss();
        state = destinationState;
        fabBack.setVisibility(View.VISIBLE);
    }


    private void getPlaceDetails(final String state) {
        final LatLng latLng = mMap.getCameraPosition().target;
        disposableSingleObserver = placeViewModel.getDetails(latLng.latitude, latLng.longitude)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Place>() {
                    @Override
                    public void onSuccess(Place place) {

                        if (state.equals(originState)) {
                            originFragment.setAddress(place.getPlaceAddress());
                        } else {
                            mMap.addMarker(new MarkerOptions().position(latLng));
                            setDestinationFragment(place.getPlaceAddress());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

    @Override
    public void onCameraIdle() {
        if (state.equals(originState)) {
            getPlaceDetails(originState);
        }
    }

    @Override
    public void onCameraMove() {
        if (state.equals(originState)) {
            originFragment.setLoading();

            //dispose old request
            if (disposableSingleObserver != null && !disposableSingleObserver.isDisposed()) {
                disposableSingleObserver.dispose();
            }
        }
    }

    @Override
    public void onAccpetClick() {
        state = destinationState;
        getPlaceDetails(destinationState);
    }

    private boolean backToOriginState() {
        if (state.equals(destinationState)) {
            setOriginFragment();
            getPlaceDetails(originState);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        if (!backToOriginState()) {
            super.onBackPressed();
        }else{
            mMap.clear();
        }
    }
}
