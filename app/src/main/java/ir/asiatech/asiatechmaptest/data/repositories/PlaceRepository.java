package ir.asiatech.asiatechmaptest.data.repositories;

import com.google.android.gms.common.api.Api;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import ir.asiatech.asiatechmaptest.data.ApiClient;
import ir.asiatech.asiatechmaptest.data.Services;
import ir.asiatech.asiatechmaptest.data.model.Place;

public class PlaceRepository {

    public Single<Place> getPlaceDetails(double lat, double lng) {
        String format = "jsonv2";
        return ApiClient.getRetrofit().create(Services.PlaceDetails.class).getPlaceDetails(format, lat, lng);
    }
}
