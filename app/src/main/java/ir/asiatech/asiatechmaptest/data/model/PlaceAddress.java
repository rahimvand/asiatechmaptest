package ir.asiatech.asiatechmaptest.data.model;

import com.google.gson.annotations.SerializedName;

public class PlaceAddress {
    @SerializedName("neighbourhood")
    private String neighbourhood;
    @SerializedName("suburb")
    private String suburb;
    @SerializedName("road")
    private String road;


    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }
}
