package ir.asiatech.asiatechmaptest.data.viewmodel;

import androidx.lifecycle.ViewModel;
import io.reactivex.Single;
import ir.asiatech.asiatechmaptest.data.model.Place;
import ir.asiatech.asiatechmaptest.data.repositories.PlaceRepository;

public class PlaceViewModel extends ViewModel {

    private PlaceRepository repo = new PlaceRepository();

    public Single<Place> getDetails(double lat, double lng){
        return repo.getPlaceDetails(lat, lng);
    }


}
