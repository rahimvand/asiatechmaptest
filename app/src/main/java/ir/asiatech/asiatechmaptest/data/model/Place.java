package ir.asiatech.asiatechmaptest.data.model;

import com.google.gson.annotations.SerializedName;

public class Place {

    @SerializedName("lat")
    private double lat;
    @SerializedName("lon")
    private double lng;
    @SerializedName("address")
    private PlaceAddress placeAddress;


    public PlaceAddress getPlaceAddress() {
        return placeAddress;
    }

    public void setPlaceAddress(PlaceAddress placeAddress) {
        this.placeAddress = placeAddress;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
