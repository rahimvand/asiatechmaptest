package ir.asiatech.asiatechmaptest.data;

import io.reactivex.Single;
import ir.asiatech.asiatechmaptest.data.model.Place;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Services {

    interface PlaceDetails{

        @GET(ApiClient.GET_PLACE_DETAILS)
        Single<Place> getPlaceDetails(@Query("format") String format,
                                      @Query("lat") double lat,
                                      @Query("lon") double lng);

    }


}
